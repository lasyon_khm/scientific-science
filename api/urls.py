from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import SimpleRouter

from api.views import main_view, ArticleModelViewSet, CommentModelViewSet

router = SimpleRouter()
router.register('articles', ArticleModelViewSet, basename='articles')
router.register('comments', CommentModelViewSet, basename='comments')

urlpatterns = [
   path('', main_view),
   path('token/', obtain_auth_token),
   *router.urls
]