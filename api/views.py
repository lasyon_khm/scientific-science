from rest_framework.decorators import api_view, permission_classes
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from api.serializers import ArticleSerializer, CommentSerializer
from web.models import Article, Comment


@api_view(["GET"])
@permission_classes([])
def main_view(request):
    return Response({"status": "ok"})


class ArticleModelViewSet(ModelViewSet):
    serializer_class = ArticleSerializer

    def get_queryset(self):
        return Article.objects.all().select_related("user").filter(user=self.request.user)


class CommentModelViewSet(ListModelMixin, GenericViewSet):
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.all().select_related("user").filter(user=self.request.user)
