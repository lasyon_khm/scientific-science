from rest_framework import serializers

from web.models import User, Article, Comment


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')



class ArticleSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    def save(self, **kwargs):
        self.validated_data['user_id'] = self.context['request'].user.id
        return super().save(**kwargs)

    class Meta:
        model = Article
        fields = ('id', 'title', 'text', 'user')


class CommentSerializer(serializers.ModelSerializer):
    def save(self, **kwargs):
        self.validated_data['user_id'] = self.context['request'].user.id
        return super().save(**kwargs)

    class Meta:
        model = Comment
        fields = ('id', 'text', 'user')
