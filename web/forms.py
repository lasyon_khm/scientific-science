from django import forms
from django.contrib.auth import get_user_model

from web.models import Article, Comment

User = get_user_model()


class RegistrationForm(forms.ModelForm):
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password'] != cleaned_data['password2']:
            self.add_error('password', 'Пароли не совпадают')
        return cleaned_data


    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')


class AuthForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class ArticleForm(forms.ModelForm):
    def save(self, commit=True):
        self.instance.user = self.initial['user']
        return super().save(commit)

    class Meta:
        model = Article
        fields = ('title', 'text', 'image')


class CommentForm(forms.ModelForm):
    def save(self, commit=True):
        self.instance.user = self.initial['user']
        self.instance.article_id = self.initial['article_id']
        return super().save(commit)

    class Meta:
        model = Comment
        fields = ('text',)


class ArticleFilterForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Поиск"}), required=False)


class ImportForm(forms.Form):
    file = forms.FileField()