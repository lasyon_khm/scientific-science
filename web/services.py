import csv

from scientific_science.redis import get_redis_client
from web.models import Article


def filter_articles(articles_qs, filters: dict):
    if filters['search']:
        articles_qs = articles_qs.filter(title__icontains=filters['search'])

    return articles_qs


def export_articles_csv(articles_qs, response):
    writer = csv.writer(response)
    writer.writerow(("title", "text"))

    for article in articles_qs:
        writer.writerow((
            article.title, article.text
        ))

    return response


def import_articles_from_csv(file, user_id):
    strs_from_file = (row.decode() for row in file)
    reader = csv.DictReader(strs_from_file)

    articles = []
    for row in reader:
        articles.append(Article(
            title=row['title'],
            text=row['text'],
            user_id=user_id
        ))

    Article.objects.bulk_create(articles)


def get_stat():
    redis = get_redis_client()
    keys = redis.keys("stat_*")
    return [(key.decode().replace("stat_", ""), redis.get(key).decode()) for key in keys]
