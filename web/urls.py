from django.urls import path

from web.views import main_view, registration_view, auth_view, logout_view, article_edit_view, articles_view, \
    one_article_view, comments_view, article_delete_view, import_view, stat_view

urlpatterns = [
    path('', main_view, name='main'),
    path('import/', import_view, name='import'),
    path('stat/', stat_view, name='stat'),
    path('registration/', registration_view, name='registration'),
    path('auth/', auth_view, name='auth'),
    path('logout/', logout_view, name='logout'),
    path('articles/', articles_view, name='articles'),
    path('article/<int:id>', one_article_view, name='one_article'),
    path('article/add/', article_edit_view, name='article_add'),
    path('article/<int:id>/edit/', article_edit_view, name='article_edit'),
    path('article/<int:id>/delete/', article_delete_view, name='article_delete'),
    path('comments/', comments_view, name='comments')
]
