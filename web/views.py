from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.views.decorators.cache import cache_page

from web.forms import RegistrationForm, AuthForm, ArticleForm, CommentForm, ArticleFilterForm, ImportForm
from web.models import Article, Comment
from web.services import filter_articles, export_articles_csv, import_articles_from_csv, get_stat

User = get_user_model()


def main_view(request):
    return render(request, 'web/main.html')


@cache_page(10)
@login_required
def articles_view(request):
    articles = Article.objects.all().order_by('title').select_related("user")

    filter_form = ArticleFilterForm(request.GET)
    filter_form.is_valid()

    articles = filter_articles(articles, filter_form.cleaned_data)

    total_count = articles.count()
    page_number = request.GET.get("page", 1)
    paginator = Paginator(articles, per_page=10)

    if request.GET.get('export') == 'csv':
        response = HttpResponse(
            content_type='text/csv',
            headers={"Content-Disposition": "attachment; filename=articles.csv"}
        )
        return export_articles_csv(articles, response)

    return render(request, 'web/articles.html', {
        'articles': paginator.get_page(page_number),
        'filter_form': filter_form,
        'total_count': total_count
    })


@login_required
def article_delete_view(request, id):
    article = get_object_or_404(Article, id=id) if id is not None else None
    article.delete()
    return redirect('main')


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email']
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
    return render(request, 'web/registration.html', {
        'form': form, 'is_success': is_success
    })


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, 'Введены неверные данные')
            else:
                login(request, user)
                return redirect('main')
    return render(request, 'web/auth.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('main')


@login_required
def one_article_view(request, id):
    comments = Comment.objects.filter(article_id=id)
    article = get_object_or_404(Article, id=id) if id is not None else None

    form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(data=request.POST, initial={'user': request.user, 'article_id': id})
        if form.is_valid():
            form.save()

    return render(request, 'web/one_article.html', {
        'article': article,
        'comments': comments,
        'form': form
    })


@login_required
def article_edit_view(request, id=None):
    article = get_object_or_404(Article, id=id) if id is not None else None
    form = ArticleForm(instance=article)
    if request.method == 'POST':
        form = ArticleForm(data=request.POST, files=request.FILES, instance=article, initial={'user': request.user})
        if form.is_valid():
            form.save()
            return redirect('articles')
    return render(request, 'web/article_form.html', {'form': form})


@login_required
def comments_view(request):
    comments = Comment.objects.all().order_by('text')
    return render(request, 'web/comments.html', {
        'comments': comments
    })


@login_required
def import_view(request):
    if request.method == 'POST':
        form = ImportForm(files=request.FILES)
        if form.is_valid():
            import_articles_from_csv(form.cleaned_data['file'], request.user.id)
            return redirect("main")

    return render(request, "web/import.html", {
        "form": ImportForm()
    })


@login_required
def stat_view(request):
     return render(request, "web/stat.html", {
        "results": get_stat()
    })
