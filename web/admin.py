from django.contrib import admin

from web.models import Article, Comment


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'title', 'text')
    search_fields = ('id', 'title')
    ordering = ('title',)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'text')
    search_fields = ('id', 'text')
    ordering = ('user',)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment, CommentAdmin)
