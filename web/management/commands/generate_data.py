import random
import string

from django.core.management import BaseCommand

from web.models import Article, User


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        user = User.objects.last()
        letters = string.ascii_lowercase

        for i in range(30):
            article_title = ''.join(random.choice(letters) for i in range(50))
            article_text = ''.join(random.choice(letters) for i in range(50))

            Article.objects.create(
                title=article_title,
                text=article_text,
                user=user
            )
