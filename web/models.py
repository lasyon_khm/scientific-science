from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Article(models.Model):
    title = models.CharField(max_length=256, verbose_name='Название')
    text = models.CharField(max_length=256, verbose_name='Содержание')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    image = models.ImageField(upload_to='articles/', null=True, blank=True, verbose_name='Картинка')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Статья',
        verbose_name_plural = 'Статьи'


class Comment(models.Model):
    text = models.CharField(max_length=256, verbose_name='Комментарий')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name='Статья')

    class Meta:
        verbose_name = 'Комментарий',
        verbose_name_plural = 'Комментарии'
