# Scientific science

## Запуск проекта для разработки

- `python -m venv venv` - создание виртуального окружения
- `venv/Scripts/activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установка зависимостей
- `docker-compose up -d` - запустить дополнительные сервисы в Docker. Если у вас нет Docker, установите
    - [PostgreSQL](https://www.postgresql.org/)
- `python manage.py migrate` - применить миграции
- `python manage.py runserver` - запустить сервер для разработки на http://127.0.0.01:8000